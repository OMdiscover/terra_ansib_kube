###
# Création des instances
###

resource "openstack_compute_instance_v2" "bastion" {
  name            = var.name_instance_bastion
  image_name  = "imta-docker"
  flavor_name = "m1.medium"
  key_pair        = var.key_pair
  security_groups = [var.group_bastion,var.secgroup_internal]
  network {
    name = var.network_name
  } 
  provisioner "local-exec" {
    command = "echo bastion ${openstack_networking_floatingip_v2.admin.address} >> ip_public_nodes.txt"
  }
  depends_on = [
    openstack_networking_subnet_v2.internal_subnet,
    openstack_networking_secgroup_v2.secgroup_bastion,
    openstack_networking_secgroup_v2.secgroup_internal_subnet,
    openstack_networking_secgroup_v2.secgroup_application,
  ]
}

resource "null_resource" "hello" {
  
  provisioner "local-exec" {
    command = "echo node02 ${openstack_networking_floatingip_v2.application.address} >> ip_public_nodes.txt"
  }
  depends_on = [
      openstack_networking_subnet_v2.internal_subnet,
      openstack_networking_secgroup_v2.secgroup_application,
      openstack_networking_secgroup_v2.secgroup_internal_subnet,
      ]

}




###
# Création des noeuds
###

resource "openstack_compute_instance_v2" "server" {
    for_each = toset(["node01","node02","node03","node04"])
    name         =  each.key
    image_name   = "imta-ubuntu20"
    flavor_name  = "s20.medium" 
    key_pair        = var.key_pair_2
    security_groups = [var.secgroup_app,var.secgroup_internal]
    network {
    name = var.network_name
    }
    depends_on = [
            openstack_networking_subnet_v2.internal_subnet,
            openstack_networking_secgroup_v2.secgroup_application,
            null_resource.copy,
            openstack_networking_secgroup_v2.secgroup_internal_subnet,
        ]

    provisioner "local-exec" {
    command = "echo ${each.key} ${self.access_ip_v4}  >> ip_private_nodes.txt"
    }


}

###
# Création de l' adresse IP flottante pour la machine bastion
###

resource "openstack_networking_floatingip_v2" "admin" {
  pool = var.external_network
  description = "admin"
}

###
# Création d'une adresse IP flottante pour le node2
###

resource "openstack_networking_floatingip_v2" "application" {
  pool = var.external_network
  description = "application"
}



###
# Association de l'adresse IP flottante à la machine bastion
###

resource "openstack_compute_floatingip_associate_v2" "admin" {
  floating_ip = "${openstack_networking_floatingip_v2.admin.address}"
  instance_id = "${openstack_compute_instance_v2.bastion.id}"
  depends_on = [
        openstack_compute_instance_v2.bastion,
    ]
}

###
# Association de l'adresse IP flottante au node2
###

resource "openstack_compute_floatingip_associate_v2" "application" {
  floating_ip = "${openstack_networking_floatingip_v2.application.address}"
  instance_id = "${openstack_compute_instance_v2.server["node02"].id}"
  depends_on = [
        openstack_compute_instance_v2.server,
    ]
}

###
# Création d' un réseau
###

resource "openstack_networking_network_v2" "internal_network" {
  name                = var.network_name
  admin_state_up      = true
}

###
# Création d'un sous-réseau
###

resource "openstack_networking_subnet_v2" "internal_subnet" {
  name            = var.subnet_name
  network_id      = "${openstack_networking_network_v2.internal_network.id}"
  ip_version      = var.ip_version
  cidr            = var.subnet_ip_range
  dns_nameservers = var.dns    
}

###
# Create  router 
###

resource "openstack_networking_router_v2" "internal_router" {
  name                = var.router_name
  external_network_id = "${ data.openstack_networking_network_v2.ext_network.id}"
  depends_on = [
        openstack_networking_subnet_v2.internal_subnet,
    ]
}
###
# Create Interface between router and subnet
###

resource "openstack_networking_router_interface_v2" "internal_router_interface_1" {
  router_id = "${openstack_networking_router_v2.internal_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.internal_subnet.id}"
  depends_on = [
        openstack_networking_router_v2.internal_router,
    ]
}

###
# Create security group of bastion
###

resource "openstack_networking_secgroup_v2" "secgroup_bastion" {
  name        = var.group_bastion
  description = "group pour bastion"
}

###
# Create security group of app-server
###

resource "openstack_networking_secgroup_v2" "secgroup_application" {
  name        = var.secgroup_app
  description = "groupe pour app-server"
}

###
# Create security group for internal subnet
###

resource "openstack_networking_secgroup_v2" "secgroup_internal_subnet" {
  name        = var.secgroup_internal
  description = "groupe pour les machines du sous-réseau"
}

###
# Create rules of secgroup_internal_subnet
###

# incoming TCP rule
resource "openstack_networking_secgroup_rule_v2" "secgroup_TCP_ingress_rule_internal_subnet" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = var.subnet_ip_range
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_internal_subnet.id}"

}

# outgoing TCP rule
resource "openstack_networking_secgroup_rule_v2" "secgroup_TCP_egress_rule_internal_subnet" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = var.subnet_ip_range
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_internal_subnet.id}"

}

# incoming UDP rule
resource "openstack_networking_secgroup_rule_v2" "secgroup_UDP_ingress_rule_internal_subnet" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  remote_ip_prefix  = var.subnet_ip_range
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_internal_subnet.id}"

}

# outgoing UDP rule
resource "openstack_networking_secgroup_rule_v2" "secgroup_UDP_egress_rule_internal_subnet" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "udp"
  remote_ip_prefix  = var.subnet_ip_range
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_internal_subnet.id}"

}

###
# Create rules of security group for bastion
###

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_bastion" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_bastion.id}"
}

###
# Create rules of security group for servers
###

# incoming HTTP rule
resource "openstack_networking_secgroup_rule_v2" "secgroup_HTTP_ingress_rule_application" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_application.id}"
}

# incoming HTTPS rule
resource "openstack_networking_secgroup_rule_v2" "secgroup_HTTPS_ingress_rule_application" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_application.id}"
}






















