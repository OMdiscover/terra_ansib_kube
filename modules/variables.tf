variable "ip_version" {
    type = string 
    default = "4"    
}

variable "external_network" {
  type    = string
  default = "external"
}

variable "router_name" {
  type    = string
  default = "router_tf_fr"
}

variable "network_name" {
  type    = string
  default = "network_tf_fr"
}

variable "subnet_name" {
  type    = string
  default = "subnet_tf_fr"
}

variable "subnet_ip_range" {
  type    = string
  default = "192.168.61.0/24"
}

variable "group_bastion" {
    type = string
    default = "secgroup_bastion"
}

variable "secgroup_app" {
    type = string
    default = "secgroup_application"
}

variable "name_instance_bastion" {
    type = string
    default = "bastion"
}

variable "key_pair" {
    type = string
    default = "demo"
}

variable "secgroup_internal" {
    type = string
    default = "secgroup_internal"
}

variable "dns" {
    type = list(string)
    default = ["192.44.75.10"]
}

variable "path_key_pair" {
  type = string
  default = "/home/user/Téléchargements/demo.pem"
}

variable "path_key_pair_2" {
  type = string
  default = "/home/ubuntu/.ssh"
}

variable "path_key_pair_2_local" {
  type = string
  default = "/home/user/.ssh"
}

variable "key_pair_2" {
  type = string
  default = "key"
}
