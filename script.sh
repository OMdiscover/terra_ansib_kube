### Settings configuration cluster
sed -i s/"enable_nodelocaldns: true"/"enable_nodelocaldns: false"/g inventory/vapormap_cluster/group_vars/k8s_cluster/k8s-cluster.yml
sed -i s/"metrics_server_enabled: false"/"metrics_server_enabled: true"/g inventory/vapormap_cluster/group_vars/k8s_cluster/addons.yml
sed -i s/"ingress_nginx_enabled: false"/"ingress_nginx_enabled: true"/g inventory/vapormap_cluster/group_vars/k8s_cluster/addons.yml
sed -i s/"# ingress_nginx_namespace: \"ingress-nginx\""/"ingress_nginx_namespace: \"ingress-nginx\""/g inventory/vapormap_cluster/group_vars/k8s_cluster/addons.yml
sed -i s/"# ingress_nginx_insecure_port: 80"/"ingress_nginx_insecure_port: 80"/g inventory/vapormap_cluster/group_vars/k8s_cluster/addons.yml
sed -i s/"# ingress_nginx_secure_port: 443"/"ingress_nginx_secure_port: 443"/g inventory/vapormap_cluster/group_vars/k8s_cluster/addons.yml
### Settings configuration proxy
sed -i s/"# cloud_provider:"/"cloud_provider: external"/g inventory/vapormap_cluster/group_vars/all/all.yml
sed -i s/"# external_cloud_provider:"/"external_cloud_provider: openstack"/g inventory/vapormap_cluster/group_vars/all/all.yml
sed -i s/"# http_proxy: \"\""/"http_proxy: \"http:\/\/proxy.enst-bretagne.fr:8080\""/g inventory/vapormap_cluster/group_vars/all/all.yml
sed -i s/"# https_proxy: \"\""/"https_proxy: \"http:\/\/proxy.enst-bretagne.fr:8080\""/g inventory/vapormap_cluster/group_vars/all/all.yml
sed -i s/"# no_proxy: \"\""/"no_proxy: \"127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr\""/g inventory/vapormap_cluster/group_vars/all/all.yml
# ansible.cfg
sed -i s/"ansible_ssh_args = -o ControlMaster=auto -o ControlPersist=30m -o ConnectionAttempts=100 -o UserKnownHostsFile=\/dev\/null"/"ansible_ssh_args = -o UserKnownHostsFile=\/dev\/null  -o ControlMaster=auto  -o ForwardAgent=yes  -o ControlPersist=60s"/g ./ansible.cfg
echo "[persistent_connection]">>ansible.cfg
echo "connect_timeout = 120">>ansible.cfg
# sed -i '/\[\defaults\]/a forks = 1' ansible.cfg
## proxy configurations
echo 'ftp_proxy: "http://proxy.enst-bretagne.fr:8080"'  >> inventory/vapormap_cluster/group_vars/all/all.yml
## enbled cinder
sed -i s/"# cinder_csi_enabled: true"/"cinder_csi_enabled: true"/g inventory/vapormap_cluster/group_vars/all/openstack.yml
## enbled helm
# sed -i s/"helm_enabled: false"/"helm_enabled: true"/g inventory/vapormap_cluster/group_vars/k8s_cluster/addons.yml







