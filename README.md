# Déploiement de App-vapormap avec Terraform, Ansible et Kubernetes
---

## Descriptif du projet
Ce projet a pour but d'automatiser le déploiement de l'application app-vapormap avec  Terraform pour créer les ressources et Ansible pour le déploiement de l'application sur un cluster kubernetes déployé à l'aide de kubespray. 5 instances sont créees, et le cluster est déployé sur trois noeuds avec un volume persistant. A noter que la clé utilisée pour se connecter à Bastion est différente de clle utilisée pour se connecter aux noeuds du cluster.
```                                      
                       -----------------                                                                
Client <---------> 80 | HAPROXY/NODE 02 | ------------------>   ---------                                            
                       -----------------                       | NODE 03 |                            
                                           +---------------->   ---------
                                           |                    ---------                                           
                                           +---------------->  | NODE 01 |                                                  
                                           |                    ---------                                           
                                           |                    ---------                       
                       ---------           +---------------->  | NODE 04 |                                
Admin  <---------> 22 | BASTION | ---------+                    ---------                                                                                    
                       ---------                                  
                                                                 
```
## prérequis avant  utilisation du  dépôt

Pour utiliser la chaîne CI/CD de ce dépôt, il faut au préalable avoir :

* Un runner Gitlab avec un exécutor shell (sous ubuntu)
* Terraform et Python, python-venv installés sur le Runner
* Utilise Openstack comme Cloud Provider
* Git installé sur le Runner

## Utilisation  du dépôt

Pour utiliser le projet, il faut:

* Configurer les variables dans le fichier main.tf  pour le déploiement de l'infra avec Terraform.
```terraform
module "principal" {
    source = "./modules"
    external_network = "external"
    router_name = "router_tf_fr"
    network_name = "network_tf_fr"
    subnet_name = "subnet_tf_fr"
    subnet_ip_range = "192.168.61.0/24"
    group_bastion = "secgroup_bastion"
    secgroup_app = "secgroup_application"
    name_instance_bastion = "bastion"
    key_pair = "demo" # nom de votre clé
    key_pair_2 = "key" # nom seconde clé
    secgroup_internal = "secgroup_internal"
    dns = ["192.44.75.10"]
    path_key_pair = "/home/user/Téléchargements/demo.pem" # emplacement de votre clé privée sur la machine hébergeant terraform
    path_key_pair_2 = "/home/ubuntu/.ssh" # emplacement distant sur bastion où copié  la seconde clé
    path_key_pair_2_local = "/home/user/.ssh" # emplacement où copié de la seconde clé sur la machine hébergeant terraform
}
```
* Créer un fichier d'environnement avec le nom "**env**" dans Gitlab au niveau de :
```
Settings\CICD\Variables
```
* Puis chargez les variables d'environnement de votre fichier openrc.sh sans les conditions : 
```bash
 
export OS_AUTH_URL=##
export OS_PROJECT_ID=##
export OS_PROJECT_NAME=##
export OS_USER_DOMAIN_NAME=##
export OS_PROJECT_DOMAIN_ID="default"
# unset v2.0 items in case set
unset OS_TENANT_ID
unset OS_TENANT_NAME
# In addition to the owning entity (tenant), OpenStack stores the entity
# performing the action as the **user**.
export OS_USERNAME=##
# password
export OS_PASSWORD=##
# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME=##
# Don't leave a blank variable, unset it if it was empty
export OS_INTERFACE=public
export OS_IDENTITY_API_VERSION=3
```
* Configurer les variables d'environnement pour  personnaliser l'hôte sur lequel le déploiement s'effectuera , dans le fichier "**.gitlab-ci.yml**".

```bash
NODE_USER=ubuntu # nom user des instances cloud
KEY_PATH=/home/user/Téléchargements/demo.pem # clé privée utilisateur 
KEY_PATH_2=/home/user/.ssh/key.pem # copie en local de la seconde clé générée par terraform 
```
* La pipeline ne se lance que si le message de commit est "**prod**"

## Test de l'application

Pour tester l'application, il faut modifier le fichier "**/etc/hosts**" et y'ajouter l'adresse IP publique de l'instance "**node02**"" avec le nom de domaine "**vapormap**" comme ceci :
```bash
adresse_ip_publique_node02    vapormap
```
Il suffit ensuite d'entrer l'adresse IP publique  de l'instance "**node02**"  dans un navigateur :
```bash
adresse_ip_publique_node02
```
## WARNINGS :
* Ne pas utiliser une image ayant docker déjà installé pour les noeuds sur lesquels Kubernetes est déployé, ça apporterait des problèmes. 
* Le fichier **script.sh** regroupe la configuration pour le déploiement du cluster. Elle est faite pour un déploiement sur openstack.
* Je vous conseille d'augmenter la durée limite de l'exécution d'un job à plus d'une heure. Car le déploiement du cluster avec 3 noeuds peut atteindre 30 minutes et aller au delà d'une heure.




